package shildt_ch15_l4;

//A block lambda that computes the factorial of an int value. 

interface NumericFunc { 
int func(int n); 
} 

class BlockLambdaDemo { 
public static void main(String args[]) 
{ 

 // This block lambda computes the factorial of an int value. 
 NumericFunc factorial = (n) ->  { 
   int result = 1; 

   for(int i=1; i <= n; i++) 
     //result = i * result; 
     result *= i; 

   return result; 
 }; 

 System.out.println("The factoral of 3 is " + factorial.func(3)); 
 System.out.println("The factoral of 5 is " + factorial.func(5)); 
} 
}
//The factoral of 3 is 6
//The factoral of 5 is 120

